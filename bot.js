const tmi = require('tmi.js');
const client = new tmi.client(require("./config.js"));
const fs = require('fs');

const Queue = require('./queue.js');
const Level = require('./level.js');
const Submitter = require('./submitter');

const queue = new Queue();
let data;

client.on('message', (channel, user, message, self) => {
    if(self) return;
    let command;
    if(message.startsWith('!')) {
        if (message.includes(' ')) {
            command = message.slice(0, message.indexOf(' '));
        } else {
            command = message.slice(0, message.length);
        }
    }

    if(command)  {
        if(command==="!open"){
            if(user.badges.broadcaster==1){
                queue.acceptingLevels = true;
                client.say(channel, "The level queue is now open!");
            }else{
                client.say(channel, `/timeout ${user.username} 10`);
                client.say(channel, `\\me ${user.username} is open to being timed out :iscrea1IRL:`);
            }
        }else if(command==="!close"){
            if(user.badges.broadcaster==1){
                queue.acceptingLevels = false;
                client.say(channel, "The level queue is now closed!");
            }else {
                client.say(channel, `/timeout ${user.username} 10`);
                client.say(channel, `\\me ${user.username} is going to close their mouth for a while :iscrea1IRL:`);
            }
        }else{
            switch (command){
                case "!add":
                    if(queue.acceptingLevels){
                        let levelID = message.slice(5,16);
                        if(levelID.length==11 && message.length==16 && levelID[3]=='-' && levelID[7]=='-'){
                            let level = new Level(`${user.username}`, levelID.toUpperCase() );
                            if(queue.submitterExists(user.username)){
                                let submitterID = queue.getSubmitterID(user.username);
                                if(queue.submitters[submitterID].submissionCount<queue.settings.maxSubmissions){
                                    queue.submitters[submitterID].incrementSubmissionCount();
                                    client.say(channel, queue.add(level));
                                }else{
                                    client.say(channel, `Sorry ${user.username}, you've had the maximum number of submissions`);
                                }
                            }else{
                                let submitter = new Submitter(user.username, user.username);
                                queue.submitters.push(submitter);
                                client.say(channel,queue.add(level));
                            }
                            data=JSON.stringify(queue);
                            fs.writeFileSync('queue.json',data);
                        }
                        else{
                            client.say(channel, `Sorry ${user.username}, use the form PLY-MYY-LVL.  Or just go away...`);
                        }
                    }else{
                        client.say(channel, `Sorry ${user.username}, the queue is currently closed :/`)
                    }

                    break;
                case "!queue":
                case "!list":
                    client.say(channel, "\\me =====Current Level===== ");
                    client.say(channel, `ID: ${queue.currentLevel.levelID}`);
                    client.say(channel, `From: ${queue.currentLevel.submitterName}`);
                    if(queue.levels.length!=0){
                        client.say(channel, "\\me =====Queue=====");
                        client.say(channel, `${queue.levels.length} total levels in queue:`)
                        let queueEnd;
                        if(queue.levels.length > 10){
                            queueEnd = 10;
                        }else{
                            queueEnd = queue.levels.length;
                        }
                        for (let i = 0; i < queueEnd; i++) {
                            client.say(channel, ` ${i+1}) ${queue.levels[i].submitterName}`);
                        }
                    }
                    if(queue.levels.length>10) client.say(channel, `And ${queue.levels.length-10} others.`)
                    client.say(channel, "\\me =======================");

                    break;
                case "!next":
                    if(user.badges.broadcaster==1){

                        client.say(channel, queue.nextLevel());
                    }else{
                        client.say(channel, `/timeout ${user.username} 10`);
                        client.say(channel, `\\me ${user.username} is timed out for the next 10 seconds`);
                    }
                    data=JSON.stringify(queue);
                    fs.writeFileSync('queue.json',data);
                    break;
                case "!random":
                case "!rand":
                    if(user.badges.broadcaster==1){
                        client.say(channel, queue.randomLevel())
                    }else{
                        client.say(channel, `/timeout ${user.username} ${Math.floor(Math.random()*60)}`);
                        client.say(channel, `\\me ${user.username} is timed out for a random amount of time`);
                    }
                    data=JSON.stringify(queue);
                    fs.writeFileSync('queue.json',data);
                    break;
                case "!current":
                    if(queue.currentLevel.levelID==" "){
                        client.say(channel, "There's no current level! :(");
                    }else{
                        client.say(channel, `The current level is ${queue.currentLevel.levelID} submitted by ${queue.currentLevel.submitterName}`)
                    }
                    break;
                case "!commands":
                    queueOpen = false;
                    client.say(channel, "!add PLY-MYY-LVL, !queue, !current");
                    break;
                case "!clear":
                    if(user.badges.broadcaster==1){
                        client.say(channel, queue.clear());
                    }else {
                        client.say(channel, `/timeout ${user.username} 10`);
                        client.say(channel, `\\me ${user.username} is going to clear their head for 10 seconds`);
                    }
                    break;
                case "!replace":
                    let levelID = message.slice(9,20);
                    if(queue.submitterExists(user.username)){
                        if(levelID.length==11 && message.length==20 && levelID[3]=='-' && levelID[7]=='-') {
                            client.say(channel,queue.replace(user.username,levelID))
                        }else{
                            client.say(channel, `That is not a valid level ID :(`);
                        }
                    }else{
                        client.say(channel, `${user.username} you are not currently in the queue ;/`)
                    }

            }
        }
    }
});

client.connect();