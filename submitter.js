class Submitter{
    constructor(submitterName, submitterRole){
        this._submitterName = submitterName;
        this._submissionCount = 1;
        this._submitterRole = submitterRole;
    }
    get submitterName(){
        return this._submitterName;
    }
    get submissionCount(){
        return this._submissionCount;
    }
    get submitterRole(){
        return this._submitterRole;
    }
    incrementSubmissionCount(){
        this._submissionCount++;
    }
    decrementSubmissionCount(){
        this._submissionCount--;
    }
};

module.exports = Submitter;
