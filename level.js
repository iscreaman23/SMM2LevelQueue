class Level {
    constructor(submitterName,levelID){
        this._submitterName = submitterName;
        this._levelID = levelID;
    }
    get submitterName(){
        return this._submitterName;
    }
    get levelID(){
        return this._levelID;
    }
    set levelID(levelID){
        this._levelID = levelID;
    }
};

module.exports = Level;
