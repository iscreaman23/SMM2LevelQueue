class Queue {
    constructor(){
        this.settings = {
            maxLevels: 100,
            maxSubmissions: 3,
            maxTimePerLevel: 15
        }
        this.levels = [ ];
        this.submitters = [ ];
        this.currentLevel = {
            levelID: " ",
            submitterName: " "
        }
    }

    submitterExists(submitterName){
        let submitterFound = false
        if(this.submitters.length==0) return false;
        for(let i = 0; i<this.submitters.length; i++){
            if(this.submitters[i].submitterName===submitterName) submitterFound = true;
        }
        return submitterFound;
    }

    getSubmitterID(submitterName){
        let submitterID=0;
        for(let i = 0; i < this.submitters.length; i++){
            if(submitterName===this.submitters.submitterName) submitterID = i;
        }
        return submitterID;
    }

    add(level){
        this.levels.push(level);
        if(this.currentLevel.levelID===" " && this.currentLevel.levelID===" ") this.nextLevel();
        return  `${level.submitterName} your level ${level.levelID} has been added to the queue.`;
    }

    replace(submitterName,levelID){
        this.levels[this.findUserLevelIndex(submitterName)].levelID=levelID;
        return `${submitterName} your level ID has been replaced with ${levelID}`;
    }
    nextLevel(){
        if(this.levels.length>0) {
            this.submitters[this.getSubmitterID(this.currentLevel.submitterName)].decrementSubmissionCount();
            this.currentLevel = this.levels.shift();
            return `The next level is ${this.currentLevel.levelID} submitted by ${this.currentLevel.submitterName}`
        }else {
            return "Oh no we are out of levels!";
        }
    }

    clear(){
        this.levels = [ ];
        this.currentLevel = {
            levelID: " ",
            submitterName: " "
        }
        return 'The queue has been cleared.';
    }

    findUserLevelIndex(submitterName){
        let submitterIndex;
        for(let i = 0; i < this.levels.length; i++){
            if(this.levels[i].submitterName === submitterName) submitterIndex = i;
        }
        return submitterIndex;
    }

    randomLevel(){
        if(this.levels.length>0) {
            let randIndex = Math.floor((Math.random() * this.levels.length));
            this.submitters[this.getSubmitterID(this.currentLevel.submitterName)].decrementSubmissionCount();
            this.currentLevel = this.levels[randIndex];
            this.levels.splice(randIndex,1);
            return `The next level is ${this.currentLevel.levelID} submitted by ${this.currentLevel.submitterName}`;
        }else {
            return "Oh no we are out of levels!";
        }
    }
};

module.exports = Queue;